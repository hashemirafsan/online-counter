<?php 

class userProfile{
    public $user;
    public $timer1 = "Online";
    public $timer2 = "Offline";    
    public function userInfo(){
        include("database/db.php");
        session_start();
        if ($_SESSION['web']!="start") {
            header("location:login.php");       
        }
        else 
            {
            
                if (isset($_SESSION['id']) && isset($_SESSION['username'])) {
                    
                
                $id = $_SESSION['id'];
                //echo $id;
                //exit();
                $query = sprintf("SELECT * FROM usertable WHERE id = '%s' LIMIT 1",$mysqli->real_escape_string($id));
                $sql = $mysqli->query($query);
                 if ($sql == true){
                        date_default_timezone_set("Asia/Dacca");
                        $mysqldate = date("Y-m-d H:i:s",time()+ 60);
                        $query2 = sprintf("UPDATE usertable SET time = '%s' WHERE id = '%s'",$mysqli->real_escape_string($mysqldate),$mysqli->real_escape_string($id));
                        $sql1 = $mysqli->query($query2);
                        if ($sql1){
                            $query3 = sprintf("UPDATE usertable SET status = 1  WHERE id = '%s'",$mysqli->real_escape_string($id));
                            $sql2 = $mysqli->query($query3);
                        }
                        else{
                            echo "status is not update , check again please";
                        }
                     }

                while($row=$sql->fetch_array())
                {
                    
                    $username = $row['username'];
                    $user_id = $row['id'];
                    $status = $row['status'];
                    if ($status == 1 ){
                         echo "Name :".$username."<br>","<hr>";
                          echo "id :".$user_id."<br>","<hr>";
                            echo "status: Online";
                    }
                    else{
                         echo "Name :".$username."<br>","<hr>";
                          echo "id :".$user_id."<br>","<hr>";
                            echo "status: Offline";
                            
                    }
               
                }
               
                
                
                }
            }
            

        
        }
    }

$userPros = new userProfile;


 ?>


 <!DOCTYPE html>
<html>
<head>
  <title>Chat app</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8" async defer></script>
  <script src="/js/main.js" type="text/javascript" charset="utf-8" async defer></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(function(){  
            $("#chatData").focus(); 
          $('<audio id="chatAudio"><source src="notify.ogg" type="audio/ogg"><source src="notify.mp3" type="audio/mpeg"><source src="notify.wav" type="audio/wav"></audio>').appendTo('body');
          $("#trig").on("click",function(){
            var a = $("#chatData").val().trim();
            if(a.length > 0){
            $("#chatData").val('');   
            $("#chatData").focus(); 
             $("<li></li>").html('<img src="small.jpg"/><span>'+a+'</span>').appendTo("#chatMessages");
              $("#chat").animate({"scrollTop": $('#chat')[0].scrollHeight}, "slow");
              $('#chatAudio')[0].play();
            }
          });
        });
    </script>
    <style type="text/css">
        * { padding:0px; margin:0px; }
        body{font-family:arial;font-size:13px}
        #chatBox {width:400px; border:1px solid #000;margin:5px;}
        #chatBox > h3 { background-color:#6d84b4; padding:3px; color:#fff; }
        #chat { max-height:220px; overflow-y:auto; max-width:400px; }
        #chat > ul > li { padding:3px;clear:both;padding:4px;margin:10px 0px 5px 0px;overflow:auto }
        #chatMessages{list-style:none}

        #chatMessages > li > img { width:35px;float:left }
        #chatMessages > li > span { width:300px;float:left;margin-left:5px}
        #chatData { padding:5px; margin:5px; border-radius:5px; border:1px solid #999;width:300px }
        #trig { padding: 4px;border: solid 1px #333;background-color: #133783;color:#fff;font-weight:bold }
    </style>

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-3 user">
                <div id="load_tweets">
                   <header id="header" class="">
                        <h2 class="text-center">User Info</h2>
                    </header><!-- /header -->
               <h3 class="text-center"> <?php $userPros -> userInfo(); ?></h3>   "<br>"</div>
            </div>
            <div class="col-md-6 app">
                <div style='margin:0 auto;width:800px;'>
                <div style='margin:20px'></div>    
                <div id='chatBox' style='margin-top:20px'>
                    <h3>Chat App</h3>
                    <div id='chat'>
                        <ul id='chatMessages'>
                            <li>
                                <img src="small.jpg"/>
                                <span>Hello Friends</span>
                            </li>
                            <li>
                                <img src="small.jpg"/>
                                <span>How are you?</span>
                            </li>
                        </ul>
                    </div>
                    <input type="text" id="chatData" placeholder="Message" />
                    <input type="button" value=" Send " id="trig" />
                </div>        
                </div>
            </div>
            <div class="col-md-3 who">
                <form action="logout.php" method="POST">
                     <button name="submit" class="btn-xs btn-primary pull-right">LOG OUT</button>
                </form>
                 <div id="load_tweets"><?php include('counter-user.php');?> </div>
            </div>
        </div>
    </div>
</body>
</html>